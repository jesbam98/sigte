<?php

/**
 * Autenticación
 * @author Jesus Bautista <jesbam98@gmail.com>
 */

namespace Core;

class Authentication
{
	private $users;
	private $usernameColumn;
	private $passwordColumn;

	/**
	 * Crea una nueva instancia de la clase autenticación
	 *
	 * @param DatabaseTable $users Tabla de usuarios
	 * @param string $usernameColumn Columna referente al usuario
	 * @param string $passwordColumn Columna referente a la contraseña
	 */
	public function __construct(DatabaseTable $users, string $usernameColumn, string $passwordColumn)
	{
		session_start();
		$this->users = $users;
		$this->usernameColumn = $usernameColumn;
		$this->passwordColumn = $passwordColumn;
	}

	/**
	 * Iniciar sesión con usuario y contraseña
	 *
	 * @param string $username Usuario
	 * @param string $password Contraseña
	 * @return boolean
	 */
	public function login($username, $password)
	{
		$user = $this->users->find($this->usernameColumn, strtolower($username));

		if (!empty($user) && password_verify($password, $user[0]->{$this->passwordColumn})) {
			session_regenerate_id();
			$_SESSION['username'] = $username;
			$_SESSION['password'] = $user[0]->{$this->passwordColumn};
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * Comprobar si existe una sesión
	 *
	 * @return boolean
	 */
	public function isLoggedIn()
	{
		if (empty($_SESSION['username'])) {
			return false;
		}

		$user = $this->users->find($this->usernameColumn, strtolower($_SESSION['username']));

		if (!empty($user) && $user[0]->{$this->passwordColumn} === $_SESSION['password']) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Obtener el usuario autenticado
	 *
	 * @return \Entity\User|boolean
	 */
	public function getUser()
	{
		if ($this->isLoggedIn()) {
			return $this->users->find($this->usernameColumn, strtolower($_SESSION['username']))[0];
		} else {
			return false;
		}
	}
}
