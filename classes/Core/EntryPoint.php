<?php

/**
 * Punto de entrada a la aplicación
 * 
 * @author Jesús Bautista <jesbam98@gmail.com>
 */

namespace Core;

class EntryPoint
{
	private $route;
	private $method;
	private $routes;
	protected $layoutTemplate = "layout.html.php";
	protected $errorTemplate = "error.html.php";

	/**
	 * Crea una nueva instancia de la aplicación
	 *
	 * @param string $route Ruta de la aplicación
	 * @param string $method Método HTTP utilizado
	 * @param \Core\Routes $routes Rutas de la aplicación
	 */
	public function __construct(string $route, string $method, \Core\Routes $routes)
	{
		$this->route = $route;
		$this->routes = $routes;
		$this->method = $method;
		$this->checkUrl();
	}

	/**
	 * Revisa si el cliente ha solicitado una ruta en mayúsculas,
	 * en caso de coincidir con alguna ruta existente retorna un estado HTTP 301
	 * y redirecciona al usuario a la ruta real
	 *
	 * @return void
	 */
	private function checkUrl()
	{
		if ($this->route !== strtolower($this->route)) {
			$correctRoute = strtolower($this->route);

			if (!empty($_SERVER['QUERY_STRING'])) {
				$correctRoute .= '?' . $_SERVER['QUERY_STRING'];
			}

			http_response_code(301);
			redirect(url($correctRoute));
		}
	}

	/**
	 * Carga una plantilla
	 *
	 * @param string $templateFileName Nombre del achivo de plantilla
	 * @param array $variables Variables a utilizar dentro de la plantilla
	 * @return void
	 */
	private function loadTemplate(string $templateFileName, $variables = [])
	{
		if (!empty($variables)) {
			extract($variables);
		}

		ob_start();

		if (file_exists(__DIR__ . '/../../templates/' . $templateFileName)) {
			include  __DIR__ . '/../../templates/' . $templateFileName;
		} else {
			throw new \Exception("No se encontró la plantilla $templateFileName", 1);
		}

		return ob_get_clean();
	}

	/**
	 * Muestra un error de la aplicación
	 *
	 * @param string $title
	 * @param string $message
	 * @return void
	 */
	public function showError(string $title, string $message = '')
	{
		$output = $this->loadTemplate($this->errorTemplate, compact('title', 'message'));
		echo $this->loadTemplate($this->layoutTemplate, compact('title', 'output'));
		exit;
	}

	/**
	 * Ejecuta la aplicación
	 *
	 * @return void
	 */
	public function run()
	{
		//Obtener las rutas de la aplicación
		$routes = $this->routes->getRoutes();

		//Obtener la autenticación
		$authentication = $this->routes->getAuthentication();

		//Verificar si la ruta dada existe en las rutas de la aplicación
		if (!isset($routes[$this->route])) {
			http_response_code(404);
			$this->showError('Error 404', 'La página que intentas buscar no existe');
		}

		//Verificar si la ruta dada require autenticación y el usuario no ha iniciado sesión
		if (isset($routes[$this->route]['login']) && !$authentication->isLoggedIn()) {
			header('location: ' . url('login/error'));
		} else
			//Verificar si la ruta dada requiere ciertos permisos y el usuario no los tiene	
			if (isset($routes[$this->route]['permissions']) && !$this->routes->checkPermission($routes[$this->route]['permissions'])) {
				header('location: ' . url('login/permissionserror'));
			} else {
				// Verificar que el método HTTP seleccionado esté definido
				if (!isset($routes[$this->route][$this->method])) {
					$this->showError('Método no permitido', "El método {$this->method} no está permitido.");
				}
				//Todo está correcto...

				//Obtener el controlador
				$controller = $routes[$this->route][$this->method]['controller'];

				//Obtener la acción a realizar
				$action = $routes[$this->route][$this->method]['action'];

				//Ejecutar la acción
				$page = $controller->$action();

				$output_type = '';

				//Comprobar el tipo de contenido a mostrar
				if (isset($page['template'])) {
					$output_type = 'html';
				}

				if (isset($page['error'])) {
					$output_type = 'error';
				}

				switch ($output_type) {
					case 'html':
						//Verificar si la plantilla requiere cargar estilos y scripts
						$styles = isset($page['styles']) ? $page['styles'] : null;
						$scripts = isset($page['scripts']) ? $page['scripts'] : null;

						//Obtener el título de la página
						$title = $page['title'];

						//Verificar si la plantilla requiere variables
						if (isset($page['variables'])) {
							$output = $this->loadTemplate($page['template'], $page['variables']);
						} else {
							$output = $this->loadTemplate($page['template']);
						}

						//Imprimir la plantilla
						echo $this->loadTemplate('layout.html.php', [
							'route' => $this->route,
							'loggedIn' => $authentication->isLoggedIn(),
							'currentUser' => $authentication->getUser(),
							'title' => $title,
							'styles' => $styles,
							'output' => $output,
							'scripts' => $scripts
						]);
						break;
					case 'error':
						$this->showError($page['error']['title'], $page['error']['message']);
						break;

					default:
						echo $page;
						break;
				}
			}
	}
}
