<?php

/**
 * Tabla en base de datos
 * 
 * Esta clase representa un enlace entre una tabla de la base de datos y PHP
 */

namespace Core;

class DatabaseTable
{
	protected $pdo;
	protected $table;
	protected $primaryKey;
	protected $className;
	protected $constructorArgs;

	/**
	 * Crea una nueva instancia de una tabla en la base de datos
	 *
	 * @param \PDO $pdo Conexión de PDO
	 * @param string $table Nombre de la tabla
	 * @param string $primaryKey Llave primaria
	 * @param string $className Nombre de la clase entidad
	 * @param array $constructorArgs Argumentos de la clase entidad
	 */
	public function __construct(\PDO $pdo, string $table, string $primaryKey, string $className = '\stdClass', array $constructorArgs = [])
	{
		$this->pdo = $pdo;
		$this->table = $table;
		$this->primaryKey = $primaryKey;
		$this->className = $className;
		$this->constructorArgs = $constructorArgs;
	}

	/**
	 * Realizar una consulta
	 *
	 * @param string $sql Instrucción SQL a ejecutar
	 * @param array $parameters Parametros de la consulta
	 * @return PDOStatement|bool
	 */
	protected function query(string $sql, $parameters = [])
	{
		$query = $this->pdo->prepare($sql);
		$query->execute($parameters);
		return $query;
	}

	/**
	 * Obtiene el total de resultados a partir de una condición
	 *
	 * @param string $field Columna de la tabla
	 * @param mixed $value Valor
	 * @return void
	 */
	public function total(string $field = null, $value = null)
	{
		$sql = 'SELECT COUNT(*) FROM `' . $this->table . '`';
		$parameters = [];

		if (!empty($field)) {
			$sql .= ' WHERE `' . $field . '` = :value';
			$parameters = ['value' => $value];
		}

		$query = $this->query($sql, $parameters);
		$row = $query->fetch();
		return $row[0];
	}

	/**
	 * Busca un registro por medio de su ID
	 *
	 * @param mixed $value ID a buscar
	 * @return void
	 */
	public function findById($value)
	{
		$query = 'SELECT * FROM `' . $this->table . '` WHERE `' . $this->primaryKey . '` = :value';

		$parameters = [
			'value' => $value
		];

		$query = $this->query($query, $parameters);

		return $query->fetchObject($this->className, $this->constructorArgs);
	}

	/**
	 * Busca los resultados coincidentes a la columna y valor proporcionados
	 *
	 * @param string $column Columna 
	 * @param mixed $value Valor buscado
	 * @param string $orderBy Ordenar por
	 * @param mixed $limit Límite de resultados
	 * @param mixed $offset Resultados a desplazar
	 * @return void
	 */
	public function find(string $column, $value, string $orderBy = null, $limit = null, $offset = null)
	{
		$query = 'SELECT * FROM ' . $this->table . ' WHERE ' . $column . ' = :value';

		$parameters = [
			'value' => $value
		];

		if ($orderBy != null) {
			$query .= ' ORDER BY ' . $orderBy;
		}

		if ($limit != null) {
			$query .= ' LIMIT ' . $limit;
		}

		if ($offset != null) {
			$query .= ' OFFSET ' . $offset;
		}

		$query = $this->query($query, $parameters);

		return $query->fetchAll(\PDO::FETCH_CLASS, $this->className, $this->constructorArgs);
	}

	/**
	 * Busca los resultados coincidentes a los filtros proporcionados
	 *
	 * @param array $filters Filtros 
	 * @param string $orderBy Ordenar por
	 * @param mixed $limit Límite de resultados
	 * @param mixed $offset Resultados a desplazar
	 * @return void
	 */
	public function filter(array $filters, string $orderBy = null, $limit = null, $offset = null)
	{
		$query = 'SELECT * FROM ' . $this->table . ' WHERE ';
		
		//	Filtros:
		//	[
		//		[columna, operador, valor]
		//	]
		//
		//	Ejemplo: 
		//
		//	$filters = [
		//		['userId', '=', '1'],
		//		['teamId', '=', '2']
		//	];
		//

		// Parámetros
		$parameters = [];

		foreach ($filters as $index => $filter) {
			$param = uniqid($filter[0]);

			$query .= "{$filter[0]} {$filter[1]} :$param";

			$parameters[$param] = $filter[2];

			if ($index < (count($filters) - 1)) {
				$query .= ' AND ';
			}
		}

		if ($orderBy != null) {
			$query .= ' ORDER BY ' . $orderBy;
		}

		if ($limit != null) {
			$query .= ' LIMIT ' . $limit;
		}

		if ($offset != null) {
			$query .= ' OFFSET ' . $offset;
		}

		$query = $this->query($query, $parameters);

		return $query->fetchAll(\PDO::FETCH_CLASS, $this->className, $this->constructorArgs);
	}
	
	/**
	 * Inserta un arreglo de columnas con sus respectivos valores
	 *
	 * @param array $fields Arreglo de columnas y valores
	 * @return void
	 */
	private function insert(array $fields)
	{
		/**
		 * Ejemplo:
		 * 
		 * $fields = [
		 * 		'nombre' => 'Juan',
		 * 		'edad' => 20
		 * ];
		 */
		$query = 'INSERT INTO `' . $this->table . '` (';

		foreach ($fields as $key => $value) {
			$query .= '`' . $key . '`,';
		}

		$query = rtrim($query, ',');

		$query .= ') VALUES (';


		foreach ($fields as $key => $value) {
			$query .= ':' . $key . ',';
		}

		$query = rtrim($query, ',');

		$query .= ')';

		$fields = $this->processDates($fields);

		$this->query($query, $fields);

		return $this->pdo->lastInsertId();
	}

	/**
	 * Actualiza columnas de una tabla a partir de un arreglo de columnas y valores
	 *
	 * @param array $fields Arreglo de columnas y valores
	 * @return void
	 */
	private function update(array $fields)
	{
		$query = ' UPDATE `' . $this->table . '` SET ';

		foreach ($fields as $key => $value) {
			$query .= '`' . $key . '` = :' . $key . ',';
		}

		$query = rtrim($query, ',');

		$query .= ' WHERE `' . $this->primaryKey . '` = :primaryKey';

		//Set the :primaryKey variable
		$fields['primaryKey'] = $fields[$this->primaryKey];

		$fields = $this->processDates($fields);

		$this->query($query, $fields);
	}

	/**
	 * Elimina un registro a partir de su ID
	 *
	 * @param mixed $id ID del registro
	 * @return void
	 */
	public function delete($id)
	{
		$parameters = [':id' => $id];

		return $this->query('DELETE FROM `' . $this->table . '` WHERE `' . $this->primaryKey . '` = :id', $parameters);
	}

	/**
	 * Elimina los registros que coicidan con la columna y valor dados
	 *
	 * @param string $column Columna de la tabla
	 * @param mixed $value Valor de la columna
	 * @return void
	 */
	public function deleteWhere(string $column, $value)
	{
		$query = 'DELETE FROM ' . $this->table . ' WHERE ' . $column . ' = :value';

		$parameters = [
			'value' => $value
		];

		$query = $this->query($query, $parameters);
	}

	/**
	 * Obtiene todos los resultados de la tabla
	 *
	 * @param string $orderBy Ordenar por
	 * @param mixed $limit Límite de resultados
	 * @param mixed $offset Resultados a desplazar
	 * @return void
	 */
	public function findAll(string $orderBy = null, $limit = null, $offset = null)
	{
		$query = 'SELECT * FROM ' . $this->table;

		if ($orderBy != null) {
			$query .= ' ORDER BY ' . $orderBy;
		}

		if ($limit != null) {
			$query .= ' LIMIT ' . $limit;
		}

		if ($offset != null) {
			$query .= ' OFFSET ' . $offset;
		}

		$result = $this->query($query);

		return $result->fetchAll(\PDO::FETCH_CLASS, $this->className, $this->constructorArgs);
	}

	/**
	 * Dar formato a fechas
	 * 
	 * Esta función revisa los valores dados en busca de fechas y les da un formato
	 * válido para ser insertadas en la base de datos
	 *
	 * @param array $fields Valores a procesar
	 * @return array
	 */
	private function processDates(array $fields)
	{
		foreach ($fields as $key => $value) {
			if ($value instanceof \DateTime) {
				$fields[$key] = $value->format('Y-m-d');
			}
		}

		return $fields;
	}

	/**
	 * Guarda un registro o lo actualiza en caso de ya existir
	 *
	 * @param mixed $record Registro a insertar o actualizar
	 * @return void
	 */
	public function save($record)
	{
		$entity = new $this->className(...$this->constructorArgs);

		try {
			if (!isset($record[$this->primaryKey]) || $record[$this->primaryKey] == '') {
				$record[$this->primaryKey] = null;
			}
			$insertId = $this->insert($record);

			$entity->{$this->primaryKey} = $insertId;
		} catch (\PDOException $e) {
			$this->update($record);
		}

		foreach ($record as $key => $value) {
			if (!empty($value)) {
				$entity->$key = $value;
			}
		}

		return $entity;
	}
}
