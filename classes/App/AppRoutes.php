<?php

/**
 * Rutas de la aplicación
 */

namespace App;

use Core\DatabaseTable;
use Core\Authentication;

class AppRoutes implements \Core\Routes
{
	private $authentication;
	private $usersTable;
	private $projectsTable;
	private $teamsTable;
	private $activitiesTable;
	private $teamMembersTable;
	private $assignedProjectsTable;
	private $assignedActivitiesTable;
	private $commentsTable;
	private $revisionsTable;

	public function __construct()
	{
		// Conexión de BD
		include __DIR__ . '/../../includes/DatabaseConnection.php';

		// Definición de Modelos y Tablas
		$this->usersTable = new DatabaseTable($pdo, 'user', 'id', '\App\Entity\User', [&$this->teamsTable, &$this->teamMembersTable]);
		$this->authentication = new Authentication($this->usersTable, 'username', 'password');
		$this->activitiesTable = new DatabaseTable($pdo, 'activity', 'id', '\App\Entity\Activity', [&$this->projectsTable]);
		$this->projectsTable = new DatabaseTable($pdo, 'project', 'id', '\App\Entity\Project', [&$this->activitiesTable]);
		$this->teamsTable =  new DatabaseTable($pdo, 'team', 'id', '\App\Entity\Team', [&$this->teamMembersTable, &$this->usersTable, &$this->assignedProjectsTable]);
		$this->teamMembersTable = new DatabaseTable($pdo, 'team_member', 'userId');
		$this->assignedProjectsTable = new DatabaseTable($pdo, 'project_team', 'projectId', '\App\Entity\AssignedProject', [&$this->projectsTable, &$this->teamsTable]);
		$this->assignedActivitiesTable = new DatabaseTable($pdo, 'assigned_activity', 'id', '\App\Entity\AssignedActivity', [&$this->activitiesTable, &$this->usersTable]);
		$this->commentsTable = new DatabaseTable($pdo, 'comment', 'id', '\App\Entity\Comment', [&$this->usersTable]);
		$this->revisionsTable = new DatabaseTable($pdo, 'revision', 'id', '\App\Entity\Revision', [&$this->commentsTable]);
	}

	public function getRoutes(): array
	{
		// Definición de Controladores
		$loginController = new \App\Controllers\Login($this->authentication);
		$homeController = new \App\Controllers\Home($this->authentication);
		$projectController = new \App\Controllers\Project($this->projectsTable);
		$teamController = new \App\Controllers\Team($this->teamsTable, $this->usersTable, $this->teamMembersTable, $this->projectsTable);
		$activityController = new \App\Controllers\Activity($this->activitiesTable);
		$userController = new \App\Controllers\User($this->usersTable);
		$assignedProjectController = new \App\Controllers\AssignedProject($this->authentication, $this->assignedProjectsTable, $this->projectsTable, $this->assignedActivitiesTable);
		$assignedActivityController = new \App\Controllers\AssignedActivity($this->authentication, $this->assignedActivitiesTable);
		$revisionController = new \App\Controllers\Revision($this->authentication, $this->revisionsTable, $this->assignedActivitiesTable);
		$commentController = new \App\Controllers\Comment($this->authentication, $this->revisionsTable);

		// Definición de rutas:

		// $routes = [
		// 	'users' => [  								<--- Ruta
		// 		'GET' => [  							<--- Método HTTP (GET|POST)
		// 			'controller' => $userController,	<--- Controlador
		// 			'action' => 'index'					<--- Función
		// 		],
		// 		'login' => true,						<--- La ruta requiere autenticación (Opcional)
		// 		'permissions' => User::LEADER			<--- Nivel requerido para acceder a la ruta (Opcional)
		// 	]
		// ];

		$routes = [
			// Inicio
			'' => [
				'GET' => [
					'controller' => $homeController,
					'action' => 'index'
				],
				'login' => true
			],


			// Usuarios:
			'user/list' => [
				'GET' => [
					'controller' => $userController,
					'action' => 'list'
				],
				'login' => true
			],
			'user/edit' => [
				'GET' => [
					'controller' => $userController,
					'action' => 'edit'
				],
				'POST' => [
					'controller' => $userController,
					'action' => 'saveEdit'
				],
				'login' => true
			],
			'user/delete' => [
				'GET' => [
					'controller' => $userController,
					'action' => 'delete'
				],
				'POST' => [
					'controller' => $userController,
					'action' => 'delete'
				],
				'login' => true
			],

			// Equipos:
			'team/details' => [
				'GET' => [
					'controller' => $teamController,
					'action' => 'details'
				]
			],
			'team/list' => [
				'GET' => [
					'controller' => $teamController,
					'action' => 'list'
				],
				'login' => true
			],
			'team/edit' => [
				'GET' => [
					'controller' => $teamController,
					'action' => 'edit'
				],
				'POST' => [
					'controller' => $teamController,
					'action' => 'saveEdit'
				],
				'login' => true
			],
			'team/delete' => [
				'GET' => [
					'controller' => $teamController,
					'action' => 'delete'
				],
				'POST' => [
					'controller' => $teamController,
					'action' => 'delete'
				],
				'login' => true
			],
			'team/setleader' => [
				'POST' => [
					'controller' => $teamController,
					'action' => 'setLeader'
				],
				'login' => true
			],
			'team/addmember' => [
				'POST' => [
					'controller' => $teamController,
					'action' => 'addmember'
				],
				'login' => true
			],
			'team/deletemember' => [
				'POST' => [
					'controller' => $teamController,
					'action' => 'deleteMember'
				],
				'login' => true
			],
			'team/addproject' => [
				'POST' => [
					'controller' => $teamController,
					'action' => 'addProject'
				],
				'login' => true
			],

			// Proyectos:
			'project/list' => [
				'GET' => [
					'controller' => $projectController,
					'action' => 'list'
				],
				'login' => true
			],
			'project/edit' => [
				'GET' => [
					'controller' => $projectController,
					'action' => 'edit'
				],
				'POST' => [
					'controller' => $projectController,
					'action' => 'saveEdit'
				],
				'login' => true
			],
			'project/delete' => [
				'GET' => [
					'controller' => $projectController,
					'action' => 'delete'
				],
				'POST' => [
					'controller' => $projectController,
					'action' => 'delete'
				],
				'login' => true
			],

			// Proyectos asignados
			'project/list/assigned' => [
				'GET' => [
					'controller' => $assignedProjectController,
					'action' => 'list'
				],
				'login' => true,
				'permissions' => \App\Entity\User::LEADER
			],
			'project/assigned/details' => [
				'GET' => [
					'controller' => $assignedProjectController,
					'action' => 'details'
				],
				'login' => true,
				'permissions' => \App\Entity\User::LEADER
			],

			// Revisiones:
			'revision/list' => [
				'GET' => [
					'controller' => $revisionController,
					'action' => 'list'
				],
				'login' => true,
				'permissions' => \App\Entity\User::MEMBER
			],
			'revision/details' => [
				'GET' => [
					'controller' => $revisionController,
					'action' => 'details'
				],
				'login' => true,
				'permissions' => \App\Entity\User::MEMBER
			],
			'revision/new' => [
				'GET' => [
					'controller' => $revisionController,
					'action' => 'create'
				],
				'POST' => [
					'controller' => $revisionController,
					'action' => 'save'
				],
				'login' => true,
				'permissions' => \App\Entity\User::MEMBER
			],
			'comment/new' => [
				'POST' => [
					'controller' => $commentController,
					'action' => 'new'
				],
				'login' => true,
				'permissions' => \App\Entity\User::MEMBER
			],

			// Actividades:
			'activity/details' => [
				'GET' => [
					'controller' => $activityController,
					'action' => 'details',
				],
				'login' => true
			],
			'activity/delete' => [
				'POST' => [
					'controller' => $activityController,
					'action' => 'delete'
				],
				'login' => true,
				'permissions' => \App\Entity\User::SUPERVISOR
			],
			'assignedactivity/create' => [
				'POST' => [
					'controller' => $assignedActivityController,
					'action' => 'create'
				],
				'login' => true,
				'permissions' => \App\Entity\User::LEADER
			],
			'activity/assigned/status' => [
				'POST' => [
					'controller' => $assignedActivityController,
					'action' => 'changeStatus'
				],
				'login' => true,
				'permissions' => \App\Entity\User::LEADER
			],
			'activity/list/me' => [
				'GET' => [
					'controller' => $assignedActivityController,
					'action' => 'list'
				],
				'login' => true
			],

			// Autenticación
			'login/error' => [
				'GET' => [
					'controller' => $loginController,
					'action' => 'error'
				]
			],
			'login/permissionserror' => [
				'GET' => [
					'controller' => $loginController,
					'action' => 'permissionsError'
				]
			],
			'login/success' => [
				'GET' => [
					'controller' => $loginController,
					'action' => 'success'
				],
				'login' => true
			],
			'logout' => [
				'GET' => [
					'controller' => $loginController,
					'action' => 'logout'
				],
				'login' => true
			],
			'login' => [
				'GET' => [
					'controller' => $loginController,
					'action' => 'loginForm'
				],
				'POST' => [
					'controller' => $loginController,
					'action' => 'processLogin'
				]
			],
		];

		return $routes;
	}

	public function getAuthentication(): \Core\Authentication
	{
		return $this->authentication;
	}

	public function checkPermission($permission): bool
	{
		$user = $this->authentication->getUser();

		if ($user && $user->hasPermission($permission)) {
			return true;
		} else {
			return false;
		}
	}
}
