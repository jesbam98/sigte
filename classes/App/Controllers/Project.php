<?php

namespace App\Controllers;

use Core\DatabaseTable;

/**
 * Controlador de Proyectos
 */
class Project
{
    private $projects;

    public function __construct(DatabaseTable $projects)
    {
        $this->projects = $projects;
    }

    public function list()
    {
        $projects = $this->projects->findAll();


        return [
            'title' => 'Proyectos',
            'template' => 'projectlist.html.php',
            'variables' => compact('projects'),
        ];
    }

    public function edit()
    {
        $title = 'Crear proyecto';

        if (!empty($_GET['id'])) {
            $title = 'Editar proyecto';
            $project = $this->projects->findById($_GET['id']);
        }

        $styles = [];
        $styles[] = '<link rel="stylesheet" href="' . url('assets/css/project.css') . '">';

        $scripts = [];
        $scripts[] = '<script src="' . url('assets/js/project.js') . '"></script>';

        return [
            'title' => $title,
            'template' => 'editproject.html.php',
            'styles' => $styles,
            'scripts' => $scripts,
            'variables' => [
                'project' => $project ?? null,
                'title' => $title
            ]
        ];
    }

    public function saveEdit()
    {
        $projectData = $_POST['project'] ?? null;
        $activitiesData = $_POST['activities'] ?? null;


        $valid = true;
        $errors = [];

        if ($projectData == null) {
            $valid = false;
            $errors[] = "Por favor, llene los campos.";
        } else if (empty($projectData['name'])) {
            $valid = false;
            $errors[] = "El nombre del proyecto es obligatorio.";
        }

        if ($valid) {

            $project = [];

            if (isset($projectData['id'])) {
                $project['id'] = $projectData['id'];
            }

            $project['name'] = trim($projectData['name']);

            $project['description'] = $projectData['description'] ?? '';

            $projectEntity = $this->projects->save($project);

            if ($activitiesData != null && is_array($activitiesData)) {
                foreach ($activitiesData as $activityData) {
                    $activity = [];
                    $activity['id'] = $activityData['id'] ?? null;
                    $activity['name'] = $activityData['name'] ?? '';
                    $activity['description'] = $activityData['description'] ?? '';
                    $projectEntity->addActivity($activity);
                }
            }

            redirect(url('project/list'));
        } else {
            $title = 'Crear proyecto';

            if (!empty($_GET['id'])) {
                $title = 'Editar proyecto';
                $project = $this->projects->findById($_GET['id']);
            }

            $styles = [];
            $styles[] = '<link rel="stylesheet" href="' . url('assets/css/project.css') . '">';

            $scripts = [];
            $scripts[] = '<script src="' . url('assets/js/project.js') . '"></script>';

            return [
                'title' => $title,
                'template' => 'editproject.html.php',
                'styles' => $styles,
                'scripts' => $scripts,
                'variables' => [
                    'project' => $project ?? null,
                    'title' => $title,
                    'errors' => $errors
                ]
            ];
        }
    }

    public function delete()
    {
        $id = $_GET['id'] ?? null;

        if ($id !== null) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $this->projects->delete($id);
            } else {
                return [
                    'title' => 'Confirmar eliminación',
                    'template' => 'confirmdelete.html.php',
                    'variables' => [
                        'message' => '¿Desea eliminar este proyecto?',
                        'cancelUrl' => 'project/list'
                    ]
                ];
            }
        }
        redirect(url('project/list'));
    }
}
