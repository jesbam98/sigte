<?php

namespace App\Controllers;

use Core\DatabaseTable;

/**
 * Controlador de Equipos
 */
class Team
{
    private $teams;
    private $users;
    private $teamMembers;
    private $projects;

    public function __construct(
        DatabaseTable $teamsTable, 
        DatabaseTable $usersTable, 
        DatabaseTable $teamMembersTable, 
        DatabaseTable $projectsTable)
    {
        $this->teams = $teamsTable;
        $this->users = $usersTable;
        $this->teamMembers = $teamMembersTable;
        $this->projects = $projectsTable;
    }

    public function details()
    {
        $id = $_GET['id'] ?? null;

        if ($id != null) {
            $team = $this->teams->findById($id);

            // Obtener los proyectos que no pertenezcan a este equipo
            $_projects = $this->projects->findAll();
            $projects = [];
            foreach ($_projects as $project) {
                if (!$team->hasProject($project->id)) {
                    $projects[] = $project;
                }
            }

            // Obtener los usuarios que no pertenezcan a este equipo
            $_users = $this->users->findAll();
            $users = [];
            foreach ($_users as $user) {
                if (!$user->getTeam() && $user->hasPermission(\App\Entity\User::MEMBER)) {
                    $users[] = $user;
                }
            }


            if ($team) {
                return [
                    'title' => $team->name,
                    'template' => 'teamdetails.html.php',
                    'variables' => compact('team', 'users', 'projects'),
                    'styles' => [
                        '<link rel="stylesheet" href="' . url('assets/flatpickr/flatpickr.min.css') . '">',
                        '<link rel="stylesheet" href="' . url('assets/css/team.css') . '">',
                    ],
                    'scripts' => [
                        '<script src="' . url('assets/flatpickr/flatpickr.js') . '"></script>',
                        '<script src="' . url('assets/flatpickr/es.js') . '"></script>',
                        '<script src="' . url('assets/js/team.js') . '"></script>',
                    ]
                ];
            }
        }

        return [
            'title' => 'Equipo no encontrado',
            'template' => 'teamnotfound.html.php'
        ];
    }

    public function list()
    {
        $teams = $this->teams->findAll();

        return [
            'title' => 'Equipos',
            'template' => 'teamlist.html.php',
            'variables' => compact('teams')
        ];
    }

    public function edit()
    {
        $title = 'Crear equipo';

        if (!empty($_GET['id'])) {
            $title = 'Editar equipo';
            $team = $this->teams->findById($_GET['id']);
        }

        return [
            'title' => $title,
            'template' => 'editteam.html.php',
            'variables' => [
                'team' => $team ?? null,
                'title' => $title
            ]
        ];
    }

    public function saveEdit()
    {
        $teamData = $_POST['team'] ? array_purify(['id', 'name'], $_POST['team']) : null;

        $valid = true;
        $errors = [];

        if ($teamData == null) {
            $valid = false;
            $errors[] = 'Por favor, llene los campos.';
        } else {
            if (empty($teamData['name'])) {
                $valid = false;
                $errors[] = 'El nombre es obligatorio.';
            }
        }

        if ($valid) {
            // Guardar el equipo 
            $teamEntity = $this->teams->save($teamData);

            redirect(url('team/list'));
        } else {
            $title = 'Crear equipo';

            if (!empty($_GET['id'])) {
                $title = 'Editar equipo';
                $team = $this->teams->findById($_GET['id']);
            }

            return [
                'title' => $title,
                'template' => 'editteam.html.php',
                'variables' => [
                    'team' => $team ?? null,
                    'title' => $title,
                    'errors' => $errors
                ]
            ];
        }
    }

    public function delete()
    {
        $id = $_GET['id'] ?? null;

        if ($id !== null) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $this->teams->delete($id);
            } else {
                return [
                    'title' => 'Confirmar eliminación',
                    'template' => 'confirmdelete.html.php',
                    'variables' => [
                        'message' => '¿Desea eliminar este equipo?',
                        'cancelUrl' => 'team/list'
                    ]
                ];
            }
        }
        redirect(url('team/list'));
    }

    public function addMember()
    {
        $teamId = $_GET['team'] ?? null;
        $id = $_POST['userId'] ?? null;
        if ($id && $teamId) {
            $team = $this->teams->findById($teamId);

            if ($team) {
                $team->addMember($id);
            }
        }
        return redirect(url('team/details?id=' . $teamId));
    }

    public function deleteMember()
    {
        $teamId = $_GET['team'] ?? null;
        $id = $_POST['id'] ?? null;
        if ($id && $teamId) {
            $team = $this->teams->findById($teamId);

            if ($team) {

                if ($team->removeMember($id)) {
                    return json_encode(['success' => true]);
                }
            }
        }
        return json_encode(['success' => false]);
    }

    public function setLeader()
    {
        $teamId = $_GET['team'] ?? null;
        $id = $_POST['id'] ?? null;
        if ($id && $teamId) {
            $team = $this->teams->findById($teamId);

            if ($team) {
                if ($team->leaderId) {
                    $currentLeader = $this->users->findById($team->leaderId);
                    $memberData = [
                        'id' => $currentLeader->id,
                        'permissions' => \App\Entity\User::MEMBER
                    ];
                    $this->users->save($memberData);
                }

                $teamData = [
                    'id' => $team->id,
                    'leaderId' => $id
                ];

                $team = $this->teams->save($teamData);

                $newLeader = $this->users->findById($team->leaderId);
                $leaderData = [
                    'id' => $newLeader->id,
                    'permissions' => $newLeader->permissions += \App\Entity\User::LEADER
                ];
                $this->users->save($leaderData);
                return json_encode(['success' => true]);
            }
        }
        return json_encode(['success' => false]);
    }

    public function addProject()
    {
        $teamId = $_GET['team'] ?? null;
        $assignedProject = $_POST['project'] ?? null;

        if ($teamId != null && $assignedProject != null) {
            // Limpiar el arreglo proveniente del formulario
            $assignedProject = array_purify(['projectId', 'dueDate'], $assignedProject);

            $team = $this->teams->findById($teamId);

            if ($team) {
                $team->addProject($assignedProject);
            }
        }

        redirect(url('team/details?id=' . $teamId));
    }
}
