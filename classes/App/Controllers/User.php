<?php

namespace App\Controllers;

use Core\DatabaseTable;

/**
 * Controlador Usuarios
 */
class User
{
    private $users;

    public function __construct(DatabaseTable $usersTable)
    {
        $this->users = $usersTable;
    }

    /**
     * Mostrar el listado de usuarios
     * 
     * Este método muestra el listado de usuarios existentes en la BD
     *
     * @return void
     */
    public function list()
    {
        // Buscar todos los usuarios
        $users = $this->users->findAll();

        return [
            'title' => 'Usuarios',
            'template' => 'userlist.html.php',
            'variables' => [
                'users' => $users
            ]
        ];
    }

    /**
     * Mostrar formulario para crear o editar un usuario
     * 
     * Este método muestra el formulario para crear un usuario, y
     * en caso de que se le proporcione un ID, llena el formulario
     * con los datos del usuario
     *
     * @return void
     */
    public function edit()
    {
        $title = 'Nuevo usuario';

        if (!empty($_GET['id'])) {
            $title = 'Editar usuario';
            $user = $this->users->findById($_GET['id']);
        }

        return [
            'title' => $title,
            'template' => 'edituser.html.php',
            'variables' => [
                'title' => $title,
                'user' => $user ?? NULL,
            ]
        ];
    }

    /**
     * Guardar o editar
     * 
     * Este método guarda la información de un usuario o la edita
     * en caso de que un ID sea proporcionado
     *
     * @return void
     */
    public function saveEdit()
    {
        // Obtener los datos por POST
        $user = $_POST['user'] ?? NULL;

        // Establecer una bandera
        $valid = TRUE;

        // Definir un arreglo de errores
        $errors = [];

        // Verificar que los datos hayan sido enviados
        if ($user != NULL) {

            // Campos requeridos
            $fields = ['id', 'firstName', 'lastName', 'email', 'username', 'password', 'permissions'];

            // Filtrar el arreglo para extraer los campos requeridos
            $user = array_purify($fields, $user);

            if (empty($user['firstName'])) {
                $valid = FALSE;
                $errors[] = 'El nombre es obligatorio.';
            }

            if (empty($user['lastName'])) {
                $valid = FALSE;
                $errors[] = 'El apellido es obligatorio.';
            }

            if (empty($user['email'])) {
                $valid = FALSE;
                $errors[] = 'El correo es obligatorio.';
            } else {

                if (filter_var($user['email'], FILTER_VALIDATE_EMAIL) == FALSE) {
                    $valid = FALSE;
                    $errors[] = 'El correo es inválido.';
                } else {
                    if (empty($user['id'])) {
                        $user['email'] = strtolower($user['email']);

                        if (count($this->users->find('email', $user['email'])) > 0) {
                            $valid = FALSE;
                            $errors[] = 'El correo ya está en uso.';
                        }
                    }
                }
            }

            if (empty($user['username'])) {
                $valid = FALSE;
                $errors[] = 'El usuario es obligatorio.';
            } else {
                if (empty($user['id'])) {
                    $user['username'] = strtolower($user['username']);

                    if (count($this->users->find('username', $user['username'])) > 0) {
                        $valid = FALSE;
                        $errors[] = 'El usuario ya está en uso';
                    }
                }
            }

            if (empty($user['id'])) {
                if (empty($user['password'])) {
                    $valid = FALSE;
                    $errors[] = 'La contraseña es obligatoria.';
                } else {
                    if (strlen($user['password']) < 7) {
                        $valid = FALSE;
                        $errors[] = 'La contraseña debe ser mayor a 6 caracteres.';
                    }
                }
            }
        } else {
            $valid = FALSE;
            $errors[] = 'Por favor, llene los campos';
        }

        // Verificar que toda la información sea válida
        if ($valid) {

            // Verificar que el usuario no se esté editando
            if (empty($user['id'])) {

                // Crear un Hash de la contraseña proporcionada
                $user['password'] = password_hash($user['password'], PASSWORD_DEFAULT);
            } else {
                // Verificar que no se haya proporcionado una contraseña 
                if (empty($user['password'])) {
                    // Eliminar el índice de la contraseña para evitar que sea modificada
                    unset($user['password']);
                }
            }

            // Guardar el usuario en la BD
            $this->users->save($user);

            // Redireccionar al listado de usuarios
            redirect(url('user/list'));
        } else {
            $title = 'Nuevo usuario';

            if (!empty($_GET['id'])) {
                $title = 'Editar usuario';
                $user = $this->users->findById($_GET['id']);
            }

            return [
                'title' => $title,
                'template' => 'edituser.html.php',
                'variables' => [
                    'title' => $title,
                    'user' => $user ?? NULL,
                    'errors' => $errors
                ]
            ];
        }
    }

    /**
     * Eliminar un usuario
     * 
     * Este método muestra una confirmación de eliminación,
     * y en caso de ser aceptada lo elimina de la BD
     *
     * @return void
     */
    public function delete()
    {
        $id = $_GET['id'] ?? null;

        if ($id !== null) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $this->users->delete($id);
            } else {
                return [
                    'title' => 'Confirmar eliminación',
                    'template' => 'confirmdelete.html.php',
                    'variables' => [
                        'message' => '¿Desea eliminar este usuario?',
                        'cancelUrl' => 'user/list'
                    ]
                ];
            }
        }
        redirect(url('project/list'));
    }
}
