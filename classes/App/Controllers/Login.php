<?php

/**
 * Controlador de Inicio de Sesión
 */

namespace App\Controllers;

class Login
{
	private $authentication;
	private $redirectTo = '';
	private $login = 'login';

	public function __construct(\Core\Authentication $authentication)
	{
		$this->authentication = $authentication;
	}

	public function loginForm()
	{
		if ($this->authentication->isLoggedIn()) {
			redirect(url($this->redirectTo));
		}
		
		return [
			'template' => 'login.html.php',
			'title' => 'Iniciar Sesión',
			'styles' => [
				'<link rel="stylesheet" href="' . url('assets/css/login.css') . '">'
			]
		];
	}

	public function processLogin()
	{
		if ($this->authentication->login($_POST['username'], $_POST['password'])) {
			redirect(url($this->redirectTo));
		} else {
			return [
				'template' => 'login.html.php',
				'title' => 'Iniciar Sesión',
				'variables' => [
					'username' => $_POST['username'] ?? '',
					'error' => 'Usuario y/o contraseña inválidos.'
				],
				'styles' => [
					'<link rel="stylesheet" href="' . url('assets/css/login.css') . '">'
				]
			];
		}
	}

	public function success()
	{
		if (!$this->authentication->isLoggedIn()) {
			redirect(url('login/error'));
		}
		return ['template' => 'loginsuccess.html.php', 'title' => 'Login Successful'];
	}

	public function error()
	{
		if ($this->authentication->isLoggedIn()) {
			redirect(url($this->redirectTo));
		}
		return ['template' => 'loginerror.html.php', 'title' => 'No has iniciado sesión'];
	}

	public function permissionsError()
	{
		return ['template' => 'permissionserror.html.php', 'title' => 'Acceso denegado'];
	}

	public function logout()
	{
		unset($_SESSION);
		session_destroy();
		redirect(url($this->login));
	}
}
