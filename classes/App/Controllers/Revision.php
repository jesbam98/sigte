<?php

namespace App\Controllers;

use Core\DatabaseTable;
use Core\Authentication;

/**
 * Controlador: Revisión
 */
class Revision
{
    private $authentication;
    private $revisions;
    private $assignedActivities;

    public function __construct(
        Authentication $authentication,
        DatabaseTable $revisionsTable,
        DatabaseTable $assignedActivitiesTable
    ) {
        $this->authentication = $authentication;
        $this->revisions = $revisionsTable;
        $this->assignedActivities = $assignedActivitiesTable;
    }

    public function list()
    {
        // Actividad asignada
        $asgmtId = $_GET['asgmt'] ?? null;

        if ($asgmtId != null) {
            $assignedActivity = $this->assignedActivities->findById($asgmtId);

            if ($assignedActivity) {
                $currentUser = $this->authentication->getUser();
                $_assignedActivities = $this->assignedActivities->find('memberId', $currentUser->id);

                $valid = false;

                foreach ($_assignedActivities as $assignedAct) {
                    if ($assignedAct->id == $assignedActivity->id) {
                        $valid = true;
                        break;
                    }
                }

                if ($valid) {

                    $revisions = $this->revisions->find('assignedActivityId', $assignedActivity->id);
                    return [
                        'title' => 'Revisiones',
                        'template' => 'revisionlist.html.php',
                        'variables' => [
                            'assignedActivity' => $assignedActivity,
                            'revisions' => $revisions
                        ],
                        'scripts' => [
                            '<script src="' . url('assets/js/revision.js') . '"></script>'
                        ]
                    ];
                }
            }
        }

        return [
            'error' => [
                'title' => 'Página no encontrada',
                'message' => 'La página que intentas buscar no existe.'
            ]
        ];
    }

    public function details()
    {
        $revisionId = $_GET['id'] ?? null;

        if ($revisionId != null) {
            $revision = $this->revisions->findById($revisionId);

            if ($revision) {
            
                $currentUser = $this->authentication->getUser();

                $_assignedActivities = $this->assignedActivities->find('memberId', $currentUser->id);

                $valid = false;

                foreach ($_assignedActivities as $asgmt) {

                    if ($asgmt->id == $revision->assignedActivityId) {
                        $valid = true;
                        break;
                    }
                }

                if ($valid) {
                    
                    return [
                        'title' => 'Detalles de revisión',
                        'template' => 'revisiondetails.html.php',
                        'variables' => [
                            'revision' => $revision
                        ],
                        'styles' => [
                            '<link rel="stylesheet" href="' . url('assets/css/revision.css') . '">'
                        ]
                    ];

                }
            }  
        }

        return [
            'error' => [
                'title' => 'Página no encontrada',
                'message' => 'La página que intentas buscar no existe.'
            ]
        ];
    }

    public function create()
    {
        // Actividad asignada
        $asgmtId = $_GET['asgmt'] ?? null;

        if ($asgmtId != null) {
            $assignedActivity = $this->assignedActivities->findById($asgmtId);

            if ($assignedActivity) {

                $currentUser = $this->authentication->getUser();
                $_assignedActivities = $this->assignedActivities->find('memberId', $currentUser->id);

                $valid = false;

                foreach ($_assignedActivities as $assignedAct) {
                    if ($assignedAct->id == $assignedActivity->id) {
                        $valid = true;
                        break;
                    }
                }

                if ($valid) {
                    return [
                        'title' => 'Nueva revisión',
                        'template' => 'newrevision.html.php',
                        'variables' => [
                            'assignedActivity' => $assignedActivity
                        ]
                    ];
                }
            }
        }

        return [
            'error' => [
                'title' => 'Página no encontrada',
                'message' => 'La página que intentas buscar no existe.'
            ]
        ];
    }

    public function save()
    {
        // Actividad asignada
        $asgmtId = $_GET['asgmt'] ?? null;

        if ($asgmtId != null) {
            $assignedActivity = $this->assignedActivities->findById($asgmtId);

            if ($assignedActivity) {

                $currentUser = $this->authentication->getUser();
                $_assignedActivities = $this->assignedActivities->find('memberId', $currentUser->id);

                $valid = false;

                foreach ($_assignedActivities as $assignedAct) {
                    if ($assignedAct->id == $assignedActivity->id) {
                        $valid = true;
                        break;
                    }
                }

                if ($valid) {
                    $revision = $_POST['revision'] ?? null;
                    $revision = array_purify(['name', 'description'], $revision);

                    $uploadPath = dirname(__DIR__, 3) . '/public/' . UPLOADS_FOLDER;
                    $file = $uploadPath . basename($_FILES['file']['name']);

                    if ($_FILES['file']['error'] === UPLOAD_ERR_OK) {   
                        if (move_uploaded_file($_FILES['file']['tmp_name'], $file)) {
                            $revision['file'] = UPLOADS_FOLDER . basename($_FILES['file']['name']);
                            $revision['assignedActivityId'] = $asgmtId;
                            $this->revisions->save($revision);
                            redirect(url('revision/list?asgmt=' . $asgmtId));
                        }
                    } 
                }

            }
        }

        return [
            'error' => [
                'title' => 'Página no encontrada',
                'message' => 'La página que intentas buscar no existe.'
            ]
        ];
    }
}
