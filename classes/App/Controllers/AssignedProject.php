<?php

namespace App\Controllers;

use Core\Authentication;
use Core\DatabaseTable;

/**
 * Controlador de Proyectos asignados
 */
class AssignedProject
{
    private $authentication;
    private $assignedProjects;
    private $projects;
    private $assignedActivities;

    public function __construct(
        Authentication $authentication,
        DatabaseTable $assignedProjectsTable,
        DatabaseTable $projectsTable,
        DatabaseTable $assignedActivitiesTable
    ) {
        $this->authentication = $authentication;
        $this->assignedProjects = $assignedProjectsTable;
        $this->projects = $projectsTable;
        $this->assignedActivities = $assignedActivitiesTable;
    }

    public function list()
    {
        $leader = $this->authentication->getUser();

        return [
            'title' => 'Proyectos asignados',
            'template' => 'assignedprojectlist.html.php',
            'variables' => [
                'leader' => $leader
            ]
        ];
    }

    public function details()
    {
        $id = $_GET['id'] ?? null;
        $team = $this->authentication->getUser()->getTeam();

        if ($id != null) {
            if ($team->hasProject($id)) {
                $project = $this->projects->findById($id);
                $assignedActivities = [];

                $_assignedActivities = $this->assignedActivities->findAll();
                foreach ($_assignedActivities as  $assignedAct) {
                    if ($assignedAct->getMember()->getTeam()->id == $team->id && $assignedAct->getActivity()->projectId == $project->id) {
                        $assignedActivities[] = $assignedAct;
                    }
                }

                return [
                    'title' => $project->name,
                    'template' => 'projectdetails.html.php',
                    'variables' => [
                        'project' => $project,
                        'team' => $team,
                        'assignedActivities' => $assignedActivities
                    ],
                    'styles' => [
                        '<link rel="stylesheet" href="' . url('assets/css/assignedproject.css') . '">'
                    ],
                    'scripts' => [
                        '<script src="' . url('assets/js/assignedproject.js') . '"></script>'
                    ]
                ];
            }
        }

        return [
            'error' => [
                'title' => 'Proyecto no encontrado',
                'message' => 'El proyecto que intentas buscar no existe.'
            ]
        ];
    }
}
