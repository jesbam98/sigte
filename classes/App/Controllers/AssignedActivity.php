<?php

namespace App\Controllers;

use Core\DatabaseTable;
use Core\Authentication;

/**
 * Controlador de actividades asignadas
 */
class AssignedActivity
{
    private $authentication;
    private $assignedActivities;

    public function __construct(
        Authentication $authentication,
        DatabaseTable $assignedActivitiesTable
    ) {
        $this->authentication = $authentication;
        $this->assignedActivities = $assignedActivitiesTable;
    }

    public function create()
    {
        $assignedActivity = $_POST['assignedActivity'] ?? null;
        $projectId = $_POST['projectId'] ?? null;

        $valid = true;

        if ($assignedActivity != null) {
            $assignedActivity = array_purify(['activityId', 'memberId'], $assignedActivity);

            if (empty($assignedActivity['activityId'])) {
                $valid = false;
            }

            if (empty($assignedActivity['memberId'])) {
                $valid = false;
            }
        } else {
            $valid = false;
        }

        if ($valid) {
            $assignedActivities = $this->assignedActivities->find('activityId', $assignedActivity['activityId']);

            $previouslyAssigned = false;

            foreach ($assignedActivities as $assignedAct) {
                if ($assignedAct->memberId == $assignedActivity['memberId']) {
                    $previouslyAssigned = true;
                    break;
                }
            }
            if (!$previouslyAssigned) {
                $assignedActE = $this->assignedActivities->save($assignedActivity);
            }

            if ($projectId) {
                redirect(url('project/assigned/details?id=' . $projectId));
            } else {
                redirect(url('project/list/assigned'));
            }
        }
    }

    public function list()
    {
        $currentUser = $this->authentication->getUser();

        $assignedActivities = $this->assignedActivities->find('memberId', $currentUser->id);

        return [
            'title' => 'Mis actividades',
            'template' => 'myactivities.html.php',
            'variables' => [
                'assignedActivities' => $assignedActivities
            ]
        ];
    }

    public function changeStatus()
    {
        $id = $_POST['id'] ?? null;
        $status = $_POST['status'] ?? null;

        if ($id != null && $status != null) {
            $_assignedActivity = $this->assignedActivities->findById($id);

            if ($_assignedActivity) {
                $assignedActivity['id'] = $_assignedActivity->id;
                $assignedActivity['status'] = $status;

                if ($this->assignedActivities->save($assignedActivity)) {
                    return json_encode(['success' => true]);
                }
            }
        }
        return json_encode(['success' => false]);
    }
}
