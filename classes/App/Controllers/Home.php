<?php

namespace App\Controllers;

use Core\Authentication;

/**
 * Controlador de Inicio
 */
class Home  
{
    private $authentication;

    public function __construct(Authentication $authentication)
    {
        $this->authentication = $authentication;
    }

    public function index()
    {
        $title = 'Inicio';
        $currentUser = $this->authentication->getUser();

        // Tipos de retorno

        // - Plantillas
        // return [
		// 	'template' => 'home.html.php',          <--- Nombre de la plantilla
        // 	'title' => 'Inicio',                    <--- Título de la página
        //  'variables' => ['user' => 'Juan'],      <--- Arreglo con variables a utilizar dentro de la plantilla (Opcional)
		// 	'styles' => [                           <--- Arreglo con enlaces HTML de estilos (Opcional)
		// 		'<link rel="stylesheet" href="' . url('assets/css/home.css') . '">'
        // 	],
        //  'scripts' => [                          <--- Arreglo con enlaces HTML de scripts (Opcional)
        //      '<script src="script.js"></script>'  
        //  ]
		// ];

        // - Errores
        // return [
        //     'error' => [
        //          'title' => 'Acceso denegado',                                <--- Nombre del error
        //          'message' => 'No tienes permiso para acceder a esta página'  <--- Mensaje de error 
        //      ]
        // ];

        // - Texto
        // return 'Hola Mundo';         <--- Texto a imprimir

        return [
            'title' => $title,
            'template' => 'home.html.php',
            'variables' => [
                'currentUser' => $currentUser
            ]
        ];
    }
}
