<?php

namespace App\Controllers;

use Core\DatabaseTable;

/**
 * Controlador de Actividades
 */
class Activity
{
    private $activities;

    public function __construct(DatabaseTable $activitiesTable)
    {
        $this->activities = $activitiesTable;
    }

    public function details()
    {
        $id = $_GET['id'] ?? null;

        if ($id != null) {
            $activity = $this->activities->findById($id);

            if ($activity) {
                return [
                    'title' => $activity->name,
                    'template' => 'activitydetails.html.php',
                    'variables' => [
                        'activity' => $activity
                    ]
                ];
            }

            return [
                'error' => [
                    'title' => 'Actividad no encontrada',
                    'message' => 'La actividad que intentas buscar no existe.'
                ]
            ];
        }
    }

    public function delete()
    {
        $id = $_GET['id'] ?? null;
        $postId = $_POST['id'] ?? null;

        if ($id == $postId) {
            if ($this->activities->delete($id)) {
                return json_encode(['success' => true]);
            }
            return json_encode(['success' => false]);
        }
    }
}
