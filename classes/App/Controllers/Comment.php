<?php

namespace App\Controllers;

use Core\DatabaseTable;
use Core\Authentication;

/**
 * Controlador: Comentario
 */
class Comment  
{
	private $authentication;
	private $revisions;

	public function __construct(Authentication $authentication, DatabaseTable $revisionsTable)
	{
		$this->authentication = $authentication;
		$this->revisions = $revisionsTable;
	}

 	public function new()
 	{
 	 	$comment = $_POST['comment'] ?? null;

 	 	$comment = array_purify(['content', 'revisionId'], $comment);

 	 	if ($comment != null) {
 	 		
 	 		$valid = true;

 	 		if (empty($comment['revisionId'])) {
 	 			$valid = false;
 	 		}

 	 		if ($valid) {
 	 			
 	 			$revision = $this->revisions->findById($comment['revisionId']);

 	 			if ($revision) {
 	 				$comment['userId'] = $this->authentication->getUser()->id;
 	 				$revision->addComment($comment);
 	 				redirect(url('revision/details?id=' . $comment['revisionId']));
 	 			}

 	 		}

 	 	}


 	}   
}