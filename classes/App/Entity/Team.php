<?php

namespace App\Entity;

use Core\DatabaseTable;

/**
 * Entidad Equipo
 */
class Team
{
    private $teamMembers;
    private $users;
    private $assignedProjects;

    public function __construct(DatabaseTable $teamMembersTable, DatabaseTable $usersTable, DatabaseTable $assignedProjectsTable)
    {
        $this->teamMembers = $teamMembersTable;
        $this->users = $usersTable;
        $this->assignedProjects = $assignedProjectsTable;
    }

    public function getMembers()
    {
        $teamMembers = $this->teamMembers->find('teamId', $this->id);

        $members = [];

        foreach ($teamMembers as $teamMember) {
            $member = $this->users->findById($teamMember->userId);

            if ($member) {
                $members[] = $member;
            }
        }

        return $members;
    }

    public function addMember($userId)
    {
        $exists = $this->teamMembers->filter([['userId', '=', $userId], ['teamId', '=', $this->id]]);
        if (!$exists) {
            $teamMember = [
                'teamId' => $this->id,
                'userId' => $userId
            ];
            $this->teamMembers->save($teamMember);
        }
    }

    public function addProject($projectAssignment)
    {
        $hasProject = $this->hasProject($projectAssignment['projectId']);

        if (!$hasProject) {
            $projectAssignment['teamId'] = $this->id;
            $this->assignedProjects->save($projectAssignment);
        }
        
    }
    
    public function getAssignedProjects() 
    {
		return $this->assignedProjects->find('teamId', $this->id);
	}

    public function hasProject($projectId)
    {
        $projects = $this->assignedProjects->find('teamId', $this->id);
        
        foreach ($projects as $project) {
            if ($project->projectId == $projectId) {
                return true;
            }
        }
        return false;
    }

    public function removeMember($userId)
    {
        $teamMembers = $this->teamMembers->find('teamId', $this->id);

        foreach ($teamMembers as $teamMember) {
            if ($teamMember->userId == $userId) {
                return $this->teamMembers->delete($teamMember->userId);
            }
        }
    }
}
