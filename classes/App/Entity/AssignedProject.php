<?php

namespace App\Entity;

use Core\DatabaseTable;

class AssignedProject 
{
	private $projects;
	private $teams;

	public function __construct(DatabaseTable $projectsTable, DatabaseTable $teamsTable) 
	{
		$this->projects = $projectsTable;
		$this->teams = $teamsTable;
	}

	public function getProject()
	{
		return $this->projects->findById($this->projectId);
	}

	public function getTeam()
	{
		return $this->teams->findById($this->teamId);
	}
}
