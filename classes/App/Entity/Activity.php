<?php

namespace App\Entity;

use Core\DatabaseTable;

/**
 * Entidad: Actividad
 */
class Activity  
{
    public $id;
    public $activityId;
    public $projectId;
    public $status;
    private $projects;

    public function __construct(DatabaseTable $projectsTable)
    {
        $this->projects = $projectsTable;
    }

    public function getProject()
    {
        return $this->projects->findById($this->projectId);
    }
}
