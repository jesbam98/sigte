<?php

namespace App\Entity;

use Core\DatabaseTable;

/**
 * Entidad Usuario
 */
class User {
	/**
	 * Niveles de acceso
	 */
	const MEMBER = 1;
	const LEADER = 2;
	const SUPERVISOR = 4;
	const SUPERUSER = 8;

	public $id;
	public $firstName;
	public $lastName;
	public $username;
	public $password;
	public $permissions;
	private $teams;
	private $teamMembers;

	public function __construct(DatabaseTable $teamsTable, DatabaseTable $teamMembersTable)
	{
		$this->teams = $teamsTable;
		$this->teamMembers = $teamMembersTable;
	}

	public function getFullName()
	{
		return "$this->firstName $this->lastName";
	}

	public function getTeam()
	{
		$teamMembers = $this->teamMembers->find('userId', $this->id);
		
		if ($teamMembers) {
			$teamMember = $teamMembers[0];
			return $this->teams->findById($teamMember->teamId);
		}

		return false;
	}

	public function hasPermission($permission) {
		return $this->permissions & $permission;  
	}
}