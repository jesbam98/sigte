<?php

namespace App\Entity;

use Core\DatabaseTable;

/**
 * Entidad: Actividad asignada
 */
class AssignedActivity  
{
    private $activities;
    private $members;
    public $id;
    public $activityId;
    public $memberId;
    public $status;

    public function __construct(DatabaseTable $activitiesTable, DatabaseTable $usersTable)
    {
        $this->activities = $activitiesTable;
        $this->members = $usersTable;
    }

    public function getActivity()
    {
        return $this->activities->findById($this->activityId);
    }

    public function getMember()
    {
        return $this->members->findById($this->memberId);
    }

    public function getStatus()
    {
        if ($this->status && $this->status == 1) {
            return true;
        }
        return false;
    }
}
