<?php

namespace App\Entity;

use Core\DatabaseTable;

/**
 * Entidad Proyecto
 */
class Project  
{
    public $id;
    private $activitiesTable;

    public function __construct(DatabaseTable $activitiesTable)
    {
        $this->activitiesTable = $activitiesTable;
    }

    public function getActivities()
    {
        return $this->activitiesTable->find('projectId', $this->id);
    }

    public function addActivity($activity)
    {
        $activity['projectId'] = $this->id;
        return $this->activitiesTable->save($activity);
    }
}
