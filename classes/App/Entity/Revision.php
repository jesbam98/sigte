<?php

namespace App\Entity;

use Core\DatabaseTable;

/**
 * Entidad: Revision
 */
class Revision  
{
    private $comments;

    public function __construct(DatabaseTable $commentsTable)
    {
    	$this->comments = $commentsTable;
    }

    public function getComments()
    {
    	return $this->comments->find('revisionId', $this->id);
    }

    public function addComment($comment)
    {
    	$comment['revisionId'] = $this->id;
    	return $this->comments->save($comment);
    }
}
