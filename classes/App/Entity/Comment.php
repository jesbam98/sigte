<?php

namespace App\Entity;

use Core\DatabaseTable;

/**
 * Entidad: Comentario
 */
class Comment  
{
    private $users;

    public function __construct(DatabaseTable $usersTable)
    {
    	$this->users = $usersTable;
    }

    public function getUser()
    {
    	return $this->users->findById($this->userId);
    }
}
