<?php
/* -------------------------------------------------------------------------- */
/*                                 Utilidades                                 */
/* -------------------------------------------------------------------------- */

/**
 * Funciones de ayuda
 * 
 * @author Jesus Bautista <jesbam98@gmail.com>
 */

/**
 * Obtener URL absoluta
 *
 * @param string $route Ruta de la aplicación
 * @return string
 **/
function url(string $route = '')
{
	$route = ltrim($route, '/');

	$server_port = $_SERVER['SERVER_PORT'];
	if ($server_port == 443) {
		return 'https://' . $_SERVER['SERVER_NAME'] . '/' . URL_PREFIX . $route;
	}
	if ($server_port != 80) {
		return 'http://' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . '/' . URL_PREFIX . $route;
	}
	return 'http://' . $_SERVER['SERVER_NAME'] . '/' . URL_PREFIX . $route;
}

/**
 * Asignar u obtener un dato relámpago
 * 
 * Esta función asigna u obtiene un valor de un sólo uso,
 * es decir, es un dato que es creado y una vez que se obtiene es eliminado
 *
 * @param string $name Nombre de la variable
 * @param mixed $value Valor a asignar
 * @return mixed
 */
function flash_data(string $name, $value = null)
{
	if (session_status() == PHP_SESSION_NONE) {
		session_start();
	}

	if ($value == null) {
		if (isset($_SESSION[$name])) {
			$data = $_SESSION[$name];
			unset($_SESSION[$name]);
			return $data;
		}
		return;
	}

	return $_SESSION[$name] = $value;
}

/**
 * Asignar u obtener un dato de sesión
 * 
 * Esta función asigna u obtiene un dato de la sesión
 *
 * @param string $name Nombre de la variable
 * @param mixed $value Valor a asignar
 * @return mixed
 */
function session_data(string $name, $value = null)
{
	if (session_status() == PHP_SESSION_NONE) {
		session_start();
	}

	if ($value == null) {
		if (isset($_SESSION[$name])) {
			return $_SESSION[$name];
		}
		return;
	} else {
		return $_SESSION[$name] = $value;
	}
}

/**
 * Redireccionar a una URL dada
 * 
 * Esta función redirecciona a una URL que le sea dada
 *
 * @param string $url URL destino
 * @return void
 * @throws Exception Las cabeceras fueron enviadas
 */
function redirect($url)
{
	if (!headers_sent()) {
		header('Location: ' . $url);
		exit();
	} else {
		throw new Exception("No es posible redireccionar, las cabeceras ya fueron enviadas", 1);
	}
}

/**
 * Imprimir enlaces de paginación
 *
 * @param string $url URL a usar como enlace de paginación
 * @param integer $currentPage Número de página actual
 * @param integer $numResults Número de resultados
 * @param integer $resultsPerPage Número de resultados por página
 * @return void
 */
function pagination($url, $currentPage, $numResults, $resultsPerPage = 10)
{
	$queryString = "";

	if (!empty($_GET)) {
		foreach ($_GET as $key => $param) {
			if ($key != 'page') {
				if (!empty($_GET[$key])) {
					$queryString .= "&$key=$param";
				}
			}
		}
	}

	$queryString = htmlspecialchars($queryString, ENT_QUOTES, 'UTF-8');

	// 101 / 10
	$numPages = ceil($numResults / $resultsPerPage);

	if ($currentPage > 1) {
		echo '<a href="' . $url . '?page=' . ($currentPage - 1) . $queryString . '">&laquo;</a>';
	}

	for ($i = 1; $i <= $numPages; $i++) {
		if ($i == $currentPage) {
			echo '<a class="active" href="' . $url . '?page=' .  $i . $queryString . '">' . $i . '</a>';
		} else {
			echo '<a href="' . $url . '?page=' . $i . $queryString . '">' . $i . '</a>';
		}
	}

	if ($currentPage < $numPages) {
		echo '<a href="' . $url . '?page=' . ($currentPage + 1) . $queryString . '">&raquo;</a>';
	}
}

/**
 * Reemplazar la primera coincidencia de una cadena de texto
 *
 * @param string $search Valor a ser buscado
 * @param string $replace Valor a ser reemplazado
 * @param string $string Cadena de texto en el que se busca y sustituye
 * @return string
 */
function str_replace_once(string $search, string $replace, string $string)
{
	$occurrence = strpos($string, $search);

	if ($occurrence !== false) {

		return substr_replace($string, $replace, $occurrence, strlen($search));
	}

	return $string;
}
/**
 * Purificar un arreglo y obtener los índices proporcionados
 *
 * @param array $keys	Índices buscados
 * @param array $array	Arreglo a purificar
 * @return array
 */
function array_purify(array $keys, array $array)
{
	$filtered_array = [];

	foreach ($keys as $key) {
		if (isset($array[$key])) {
			$filtered_array[$key] = $array[$key];
		}
	}

	return $filtered_array;
}
