<?php
/**
 * Configuración de la aplicación
 */

// Habilitar modo de pruebas
define('DEBUG', TRUE);

// Indicar un prefijo de URL
define('URL_PREFIX', 'sigte/');

// Establecer zona horaria
date_default_timezone_set('America/Mexico_City');

// Definir carpeta para almacenar archivos subidos
define('UPLOADS_FOLDER', 'uploads/');

// Motor de base de datos a usar
define('DB_DRIVER', 'mysql');

// Dirección de la base de datos
define('DB_HOST', 'localhost');

// Nombre de la base de datos
define('DB_NAME', 'sigte');

// Usuario de la base de datos
define('DB_USER', 'root');

// Contraseña de la base de datos
define('DB_PASSWORD', '');