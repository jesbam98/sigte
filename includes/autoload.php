<?php
/**
 * Autocargador de clases
 * 
 * @author Jesús Bautista <jesbam98@gmail.com>
 */

 /**
  * Autocargador de clases
  *
  * @param string $className Nombre de la clase
  * @return void
  */
function autoloader(string $className) {
	$fileName = str_replace('\\', '/', $className) . '.php';

	$file = __DIR__ . '/../classes/' . $fileName;
	
	include $file;
}

spl_autoload_register('autoloader');