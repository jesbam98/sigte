-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: mysql
-- Tiempo de generación: 04-08-2019 a las 20:56:58
-- Versión del servidor: 5.7.26
-- Versión de PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sigte`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activity`
--

CREATE TABLE `activity` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `projectId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `activity`
--

INSERT INTO `activity` (`id`, `name`, `description`, `projectId`) VALUES
(3, 'Enlazar antenas', '', 1),
(4, 'Configurar Red LAN', '', 1),
(5, 'Realizar mockups', '', 2),
(6, 'CRUD de Tareas', '', 2),
(7, 'Maquetado', '', 3),
(8, 'DB', '', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `assigned_activity`
--

CREATE TABLE `assigned_activity` (
  `id` int(11) NOT NULL,
  `activityId` int(11) NOT NULL,
  `memberId` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `attachment`
--

CREATE TABLE `attachment` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `src` varchar(255) NOT NULL,
  `createdAt` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `assignedActivityId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL,
  `assignedActivityId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdAt` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `project`
--

INSERT INTO `project` (`id`, `name`, `description`) VALUES
(1, 'Antenas', 'Proyecto Ejemplo'),
(2, 'App Android', 'Realizar una app de tareas'),
(3, 'Autos MTZ', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_team`
--

CREATE TABLE `project_team` (
  `projectId` int(11) NOT NULL,
  `teamId` int(11) NOT NULL,
  `dueDate` date DEFAULT NULL,
  `score` int(11) DEFAULT '0',
  `finalFile` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `project_team`
--

INSERT INTO `project_team` (`projectId`, `teamId`, `dueDate`, `score`, `finalFile`, `status`) VALUES
(1, 11, '2019-08-02', 0, NULL, 0),
(1, 12, '2019-08-16', 0, NULL, 0),
(1, 14, '2019-08-09', 0, NULL, 0),
(2, 11, '2019-08-31', 0, NULL, 0),
(2, 12, '2019-08-16', 0, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `leaderId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `team`
--

INSERT INTO `team` (`id`, `name`, `leaderId`) VALUES
(11, 'Equipo 1', 5),
(12, 'Equipo 2', 7),
(13, 'Equipo 3', NULL),
(14, 'Equipo Alfa', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `team_member`
--

CREATE TABLE `team_member` (
  `userId` int(11) NOT NULL,
  `teamId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `team_member`
--

INSERT INTO `team_member` (`userId`, `teamId`) VALUES
(1, 11),
(5, 11),
(6, 11),
(7, 12),
(8, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstName` varchar(60) NOT NULL,
  `lastName` varchar(60) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `permissions` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `firstName`, `lastName`, `username`, `email`, `password`, `permissions`) VALUES
(1, 'Jesus', 'Bautista', 'jesbam', 'jesbam98@gmail.com', '$2y$10$MqOPXbNzdHbKpOm.VBg2NOtKJI95a7b7iLiiqsK8ZCLWJ8gdsLUEi', 1),
(5, 'Margarita', 'R', 'maguie', 'maguie@yopmail.com', '$2y$10$sKio16BGPL/91815g3Oqh.XSspmUNyVRAeyfI2E3pETAmjkkc7w9C', 2),
(6, 'Juan', 'P', 'juan123', 'juanp@yopmail.com', '$2y$10$NF8fYcbqgxzxuvNaJNpTK.gwP0a6xZVhhnMP9bT1rlnt3w5kcgGUO', 1),
(7, 'Juan', 'Camareno', 'juanca', 'juanca@yopmail.com', '$2y$10$N8mx/VMRvVsIbDPMrWjLU.6zOAEJMX68k8VukzGdcSft4fmYFHL7q', 1),
(8, 'Juan', 'Cholo', 'juancho', 'juancho@yopmail.com', '$2y$10$1ABXqCOui.ZASTEw8i.KO.YlPem.bcbpOQIp8o2dwEYbzKmlzewe2', 2),
(9, 'Guille', 'Q', 'guille2019', 'guille@yopmail.com', '$2y$10$1.lzzNFjVRDt208SWCob2O3iDsEFu7sLm3067ET487gfhxfTa07kW', 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`projectId`);

--
-- Indices de la tabla `assigned_activity`
--
ALTER TABLE `assigned_activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_id` (`activityId`),
  ADD KEY `member_id` (`memberId`);

--
-- Indices de la tabla `attachment`
--
ALTER TABLE `attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assigned_activity_id` (`assignedActivityId`) USING BTREE;

--
-- Indices de la tabla `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`userId`),
  ADD KEY `assigned_activity_id` (`assignedActivityId`) USING BTREE;

--
-- Indices de la tabla `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `project_team`
--
ALTER TABLE `project_team`
  ADD PRIMARY KEY (`projectId`,`teamId`),
  ADD KEY `team_id` (`teamId`);

--
-- Indices de la tabla `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`),
  ADD KEY `team_ibfk_1` (`leaderId`);

--
-- Indices de la tabla `team_member`
--
ALTER TABLE `team_member`
  ADD PRIMARY KEY (`userId`,`teamId`),
  ADD KEY `team_id` (`teamId`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activity`
--
ALTER TABLE `activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `assigned_activity`
--
ALTER TABLE `assigned_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `attachment`
--
ALTER TABLE `attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `activity`
--
ALTER TABLE `activity`
  ADD CONSTRAINT `activity_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `assigned_activity`
--
ALTER TABLE `assigned_activity`
  ADD CONSTRAINT `assigned_activity_ibfk_1` FOREIGN KEY (`activityId`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `attachment`
--
ALTER TABLE `attachment`
  ADD CONSTRAINT `attachment_ibfk_1` FOREIGN KEY (`assignedActivityId`) REFERENCES `assigned_activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`assignedActivityId`) REFERENCES `assigned_activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `project_team`
--
ALTER TABLE `project_team`
  ADD CONSTRAINT `project_team_ibfk_1` FOREIGN KEY (`projectId`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `project_team_ibfk_2` FOREIGN KEY (`teamId`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `team`
--
ALTER TABLE `team`
  ADD CONSTRAINT `team_ibfk_1` FOREIGN KEY (`leaderId`) REFERENCES `team_member` (`userId`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `team_member`
--
ALTER TABLE `team_member`
  ADD CONSTRAINT `team_member_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `team_member_ibfk_2` FOREIGN KEY (`teamId`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
