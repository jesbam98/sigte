<main class="page-content">

    <div class="heading heading--spaced">
        <div class="heading-title">
            <h1>Proyectos</h1>
        </div>
    </div>

    <div class="tableResponsive">
        <table class="table">
            <thead class="table-thead">
                <tr>
                    <th>Nombre</th>
                    <th>Fecha límite</th>
                    <th>Archivo Final</th>
                    <th>Calificación</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($leader->getTeam()->getAssignedProjects() as $assignedProject) : ?>
                    <tr>
                        <td><a href="<?= url('project/assigned/details?id=' . $assignedProject->getProject()->id) ?>" class="table-link"><?= $assignedProject->getProject()->name ?></a></td>
                        <td><?= DateTime::createFromFormat('Y-m-d', $assignedProject->dueDate)->format('d/m/Y') ?></td>
                        <td></td>
                        <td><?= $assignedProject->score ?></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</main>