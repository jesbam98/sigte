<main class="page-content">
    <h1><?= $title ?></h1>

    <?php if (!empty($errors)) : ?>
        <div class="errorList">
            <ul>
                <?php foreach ($errors as $error) : ?>
                    <li><?= $error ?></li>
                <?php endforeach ?>
            </ul>
        </div>
    <?php endif ?>

    <form id="teamForm" class="form" action="" method="post">
        <input type="hidden" name="team[id]" value="<?= $team->id ?? '' ?>">

        <label class="form-label" for="name">Nombre:</label>
        <input class="form-input" type="text" name="team[name]" id="name" value="<?= $team->name ?? '' ?>">

        
        <button id="btnSubmit" type="submit" class="button button--success">Guardar</button>
    </form>
</main>