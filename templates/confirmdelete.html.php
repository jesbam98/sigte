<main class="page-content">
    <form action="" method="post">

        <h2><?= $message ?></h2>

        <button class="button button--secondary" type="submit">Aceptar</button>
        <a href="<?= url($cancelUrl) ?>" class="button button--primary">Cancelar</a>
    </form>
</main>