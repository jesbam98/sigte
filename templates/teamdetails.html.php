<main class="page-content">

    <h1><?= $team->name ?></h1>

    <h2>Miembros</h2>

    <form class="memberForm" method="POST" action="<?= url('team/addmember?team=' . $team->id) ?>">
        <select name="userId" id="newMember" class="memberForm-users" required>
            <?php 
            if (count($users)):    
                foreach ($users as $user) : ?>
                <option value="<?= $user->id ?>"><?= $user->getFullName() ?></option>
            <?php 
                endforeach;
            else: ?>
                <option selected disabled>No se encontraron usuarios</option>
            <?php
            endif ?>
        </select>
        <button type="submit" id="addNew" class="button button--primary">Agregar</button>
    </form>

    <div class="tableResposive">
        <table id="members" class="table">
            <thead class="table-thead">
                <tr>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Líder
                    </th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($team->getMembers() as $member) : ?>
                    <tr id="member-<?= $member->id ?>">
                        <td>
                            <?= $member->getFullName() ?>
                        </td>
                        <td>
                            <?php if ($team->leaderId == $member->id) : ?>
                                <button class="button button--success" disabled>Seleccionado</button>
                            <?php else : ?>
                                <button data-id="<?= $member->id ?>" data-team="<?= $team->id ?>" class="setAsLeader button">Seleccionar</button>
                            <?php endif ?>
                        </td>
                        <td>
                            <button data-id="<?= $member->id ?>" data-team="<?= $team->id ?>" type="button" class="memberList-delete button button--danger">Eliminar</button>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>

    <h2>Proyectos</h2>

    <form action="<?= url('team/addproject?team=' . $team->id) ?>"" method="POST" class="projectForm">
        <select name="project[projectId]" id="newProject" class="projectForm-projects" required>
            <?php 
            if (count($projects)):
                foreach ($projects as $project) : ?>
                    <option value="<?= $project->id ?>"><?= $project->name ?></option>
            <?php 
                endforeach;
            else: ?>
                <option selected disabled>No se encontraron proyectos</option>
            <?php
            endif; ?>
        </select>
        <input type="text" name="project[dueDate]" class="projectForm-date" id="dueDate" placeholder="Fecha de entrega">
        <button type="submit" id="addNewProject" class="button button--primary">Agregar</button>
    </form>

    <div class="tableResposive">
        <table class="table">
            <thead class="table-thead">
                <tr>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Fecha de entrega
                    </th>
                    <th>
                        Estado
                    </th>
                    <th>
                        Calificación
                    </th>
                </tr>
            </thead>
            <tbody>
				<?php foreach ($team->getAssignedProjects() as $assignedProject): ?>
					<tr>
						<td>
							<?= $assignedProject->getProject()->name ?>
						</td>
						<td>
							<?= $assignedProject->dueDate ?>
						</td>
                        <td>
                            <?= $assignedProject->status ?>
                        </td>
                        <td>
                            <?= $assignedProject->score ?>
                        </td>
					</tr>
				<?php endforeach ?>
            </tbody>
        </table>
    </div>

</main>
