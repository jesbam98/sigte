<main class="page-content">
    <h2><?= $revision->name ?></h2>

    <a href="<?= url($revision->file) ?>" class="button button--secondary" download>Descargar archivo</a>

    <h3>Descripción:</h3>
    <p><?= $revision->description ?></p>

    <h3>Comentarios:</h3>
    <ul class="commentList">
        <?php foreach ($revision->getComments() as $comment): ?>
        <li class="commentList-item"><?= $comment->content ?> - <small><?= $comment->getUser()->getFullName() ?></small></li>
        <?php endforeach ?>
    </ul>

    <form class="commentForm" action="<?= url('comment/new') ?>" method="POST">
        <input type="hidden" name="comment[revisionId]" value="<?= $revision->id ?>">
        <textarea required class="commentForm-textarea" name="comment[content]" id="comment" placeholder="Comentario"></textarea>
        <button class="commentForm-button button button--success" type="submit">Enviar</button>
    </form>
</main>