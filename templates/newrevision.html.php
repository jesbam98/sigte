<main class="page-content">
    <h4>Actividad: <?= $assignedActivity->getActivity()->name ?></h4>

    <h1>Nueva revisión</h1>

    <form action="" method="post" enctype="multipart/form-data">

        <label for="name" class="form-label">Nombre:</label>
        <input required id="name" name="revision[name]" type="text" class="form-input">

        <label for="description" class="form-label">Descripción:</label>
        <textarea required name="revision[description]" id="description" class="form-textarea"></textarea>

        <label for="file" class="form-label">Archivo:</label>
        <input required type="file" name="file" id="file">
        <br>
        <br>
        <button type="submit" class="button button--success">Guardar</button>
    </form>

</main>