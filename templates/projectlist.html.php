<main class="page-content">

    <div class="heading heading--spaced">
        <div class="heading-title">
            <h1>Proyectos</h1>
        </div>
        <div class="heading-actions">
            <a href="<?= url('project/edit') ?>" class="button button--secondary">Nuevo</a>
        </div>
    </div>

    <div class="tableResponsive">
        <table class="table">
            <thead class="table-thead">
                <tr>
                    <th>Nombre</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($projects as $project) : ?>
                    <tr>
                        <td><?= $project->name ?></td>
                        <td>
                            <a href="<?= url('project/edit?id=' . $project->id) ?>" class="table-link">Editar</a>
                            |
                            <a href="<?= url('project/delete?id=' . $project->id) ?>" class="table-link">Eliminar</a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</main>