<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?= url('assets/icons/fontawesome/css/all.min.css') ?>">
    <link rel="stylesheet" href="<?= url('assets/css/style.css') ?>">
    <?php if (isset($styles) && is_array($styles)) {
        foreach ($styles as $style) {
            echo $style;
        }
    } ?>
    <title><?= $title ?> | SIGTE</title>
</head>

<body class="page">
    <?php if (isset($loggedIn) && $loggedIn == true) : ?>
        <header>
            <div class="navbar navbar--dark">
                <input type="checkbox" id="navbar__menu-toggle" hidden>
                <div class="navbar__logo">
                    <a href="<?= url('') ?>" class="navbar__logo-link">SIGTE</a>
                </div>
                <nav class="navbar__menu">
                    <a href="<?= url() ?>" class="navbar__link <?= $route == '' ? 'navbar__link--active' : '' ?>">Inicio</a>
                    
                    <?php if ($currentUser->hasPermission(\App\Entity\User::SUPERVISOR)) : ?>
                        <a href="<?= url('project/list') ?>" class="navbar__link <?= preg_match('/^project/', $route) ? 'navbar__link--active' : '' ?>">Proyectos</a>
                        <a href="<?= url('team/list') ?>" class="navbar__link <?= preg_match('/^team/', $route) ? 'navbar__link--active' : '' ?>">Equipos</a>
                        <a href="<?= url('user/list') ?>" class="navbar__link <?= preg_match('/^user/', $route) ? 'navbar__link--active' : '' ?>">Usuarios</a>
                    <?php endif ?>
                    
                    <?php if ($currentUser->hasPermission(\App\Entity\User::LEADER)) : ?>
                        <a href="<?= url('project/list/assigned') ?>" class="navbar__link <?= preg_match('/^project/', $route) ? 'navbar__link--active' : '' ?>">Mis proyectos</a>
                    <?php endif ?>

                    <?php if ($currentUser->hasPermission(\App\Entity\User::MEMBER) || $currentUser->hasPermission(\App\Entity\User::LEADER)) : ?>
                        <a href="<?= url('activity/list/me') ?>" class="navbar__link <?= preg_match('/^activity/', $route) ? 'navbar__link--active' : '' ?>">Mis actividades</a>
                    <?php endif ?>
                </nav>
                <nav class="navbar__menu navbar__menu--right">
                    <div class="navbar__dropdown dropdown">
                        <a href="#" class="dropdown__button navbar__link">
                            <?= $currentUser->username ?>
                        </a>
                        <div class="dropdown__content">
                            <a href="#" class="navbar__link">Configuración</a>
                            <a href="<?= url('logout') ?>" class="navbar__link">Salir</a>
                        </div>
                    </div>
                </nav>
                <label for="navbar__menu-toggle" class="navbar__menu-button">
                    <span class="navbar__menu-icon"></span>
                </label>
            </div>
        </header>
    <?php endif ?>

    <?= $output ?>
    <script>
        const BASE_URL = "<?= url() ?>";
    </script>
    <script src="<?= url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= url('assets/js/main.js') ?>"></script>
    <?php if (isset($scripts) && is_array($scripts)) {
        foreach ($scripts as $script) {
            echo $script;
        }
    } ?>
</body>

</html>