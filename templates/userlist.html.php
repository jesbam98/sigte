<main class="page-content">
    <div class="heading heading--spaced">
        <div class="heading-title">
            <h1>Usuarios</h1>
        </div>
        <div class="heading-actions">
            <a href="<?= url('user/edit') ?>" class="button button--secondary">Nuevo</a>
        </div>
    </div>

    <div class="tableResponsive">
        <table class="table">
            <thead class="table-thead">
                <tr>
                    <th>Nombre</th>
                    <th>Usuario</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user) : ?>
                    <tr>
                        <td><a href="<?= url('user/edit?id=' . $user->id) ?>" class="table-link"><?= $user->getFullName() ?></a></td>
                        <td><?= $user->username ?></td>
                        <td>
                            <a href="<?= url('user/edit?id=' . $user->id) ?>" class="table-link">Editar</a>
                            |
                            <a href="<?= url('user/delete?id=' . $user->id) ?>" class="table-link">Eliminar</a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</main>