<main class="page-content">
    <div class="heading heading--spaced">
        <div class="heading-title">
            <h1>Equipos</h1>
        </div>
        <div class="heading-actions">
            <a href="<?= url('team/edit') ?>" class="button button--secondary">Nuevo</a>
        </div>
    </div>

    <div class="tableResponsive">
        <table class="table">
            <thead class="table-thead">
                <tr>
                    <th>Nombre</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($teams as $team): ?>
                    <tr>
                        <td><a href="<?= url('team/details?id=' . $team->id ) ?>" class="table-link"><?= $team->name ?></a></td>
                        <td>
                            <a href="<?= url('team/edit?id=' . $team->id) ?>" class="table-link">Editar</a>
                            |
                            <a href="<?= url('team/delete?id=' . $team->id) ?>" class="table-link">Eliminar</a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</main>