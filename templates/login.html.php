<main class="page-content">
	<header>
		<nav>
			<h1 id="titulo">SIGTE</h1>
		</nav>
	</header>
	<form method="post" action="">
		<h1 class="text-center" id="titulo">Iniciar Sesión</h1>
		<div id="fondo">
			<div class="texto">
				<?php if (isset($error)) : ?>
					<div class="errorList"><?= $error; ?></div>
				<?php endif; ?>
				<fieldset class="area">
					<input class="input" type="text" id="username" name="username" autofocus value="<?= $username ?? '' ?>">
				</fieldset>
				<label class="datos" for="username">Usuario</label>
				<br>
				<fieldset class="area">
					<input class="input" type="password" id="password" name="password">
				</fieldset>
				<label class="datos" for="password">Contraseña</label>
				<br>
				<input class="button button--primary" id="boton" type="submit" name="login" value="Iniciar Sesión">
			</div>
		</div>
	</form>
</main>