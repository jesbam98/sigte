<main class="page-content">
	<h1>No has iniciado sesión</h1>
	<p>Es necesario iniciar sesión para ver esta página. <a class="link" href="<?= url('login') ?>">Presiona aquí para iniciar sesión</a></p>
</main>