<main class="page-content">

    <div class="heading">
        <div class="heading-title">
            <h1>Mis actividades</h1>
        </div>
    </div>

    <div class="tableResponsive">
        <table class="table">
            <thead class="table-thead">
                <tr>
                    <th>Nombre</th>
                    <th>Proyecto</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($assignedActivities)) : ?>
                    <?php foreach ($assignedActivities as $assignedAct) : ?>
                        <tr>
                            <td><a href="<?= url('activity/details?id=' . $assignedAct->activityId) ?>" class="table-link"><?= $assignedAct->getActivity()->name ?></a></td>
                            <td><?= $assignedAct->getActivity()->getProject()->name ?></td>
                            <td><a href="<?= url('revision/list?asgmt=' . $assignedAct->id) ?>" class="link">Ver revisiones</a></td>
                        </tr>
                    <?php endforeach ?>
                <?php else : ?>
                    <tr>
                        <td colspan="3" class="text--center">
                            No se encontraron actividades
                        </td>
                    </tr>
                <?php endif ?>
            </tbody>
        </table>
    </div>
</main>