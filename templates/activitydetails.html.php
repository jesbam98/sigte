<main class="page-content">

    <h5>Proyecto | <?= $activity->getProject()->name ?></h5>
    <h1><?= $activity->name ?></h1>

    <h3>Descripción:</h3>
    <p>
        <?php if (!empty($activity->description)) : ?>
            <?= $activity->description ?>
        <?php else : ?>
            No hay una descripción
        <?php endif ?>
    </p>
</main>