<main class="page-content">

    <h1><?= $title ?></h1>

    <?php if (!empty($errors)) : ?>
        <div class="errorList">
            <ul>
                <?php foreach ($errors as $error) : ?>
                    <li><?= $error ?></li>
                <?php endforeach ?>
            </ul>
        </div>
    <?php endif ?>

    <form class="form" action="" method="post">
        <input type="hidden" name="project[id]" value="<?= $project->id ?? '' ?>">

        <label for="name" class="form-label">Nombre:</label>
        <input type="text" class="form-input" id="name" name="project[name]" required value="<?= $project->name ?? '' ?>">

        <label for="description" class="form-label">Descripción:</label>
        <textarea name="project[description]" id="description" class="form-textarea"><?= $project->description ?? '' ?></textarea>

        <div class="heading">
            <div class="heading-title">
                <h2>Actividades:</h2>
            </div>
            <div class="heading-actions">
                <button id="addActivity" type="button" class="button button--secondary"><i class="fas fa-plus"></i></button>
            </div>
        </div>

        <?php if (!empty($project)) : ?>
        <div id="savedActivities">
            <?php foreach ($project->getActivities() as $activity) : ?>
                <div id="act<?= $activity->id ?>" class="activityForm">
                    <input type="hidden" name="activities[<?= $activity->id ?>][id]" value="<?= $activity->id ?>">
                    <input name="activities[<?= $activity->id ?>][name]" type="text" class="activityForm-name" placeholder="Nombre" value="<?= $activity->name ?>">
                    <input name="activities[<?= $activity->id ?>][description]" type="text" class="activityForm-description" placeholder="Descripción" value="<?= $activity->description ?>">
                    <button data-id="<?= $activity->id ?>" type="button" class="deleteActivity button button--danger"><i class="fas fa-times"></i></button>
                </div>
            <?php endforeach ?>
        </div>
        <?php endif ?>

        <div id="activities"></div>

        <button type="submit" class="button button--success">Guardar</button>
    </form>

</main>