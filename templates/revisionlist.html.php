<main class="page-content">
    <h1><?= $assignedActivity->getActivity()->name ?></h1>

    <div class="heading heading--spaced">
        <div class="heading-title">
            <h2>Revisiones:</h2>
        </div>
        <div class="heading-actions">
            <a href="<?= url('revision/new?asgmt=' . $assignedActivity->id) ?>" class="button button--secondary">Nueva</a>
        </div>
    </div>

    <div class="tableResponsive">
        <table class="table">
            <thead class="table-thead">
                <tr>
                    <th>Nombre</th>
                    <th>Archivo</th>
                    <th>Fecha</th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($revisions) > 0) : ?>
                    <?php foreach ($revisions as $revision) : ?>
                        <tr>
                            <td><a class="link" href="<?= url('revision/details?id=' . $revision->id) ?>"><?= $revision->name ?></a></td>
                            <td><a href="<?= url($revision->file) ?>" class="link" download>Descargar</a></td>
                            <td><?= $revision->createdAt ?></td>
                        </tr>
                    <?php endforeach ?>
                <?php else : ?>
                    <tr>
                        <td colspan="3" class="text--center">No se encontraron registros</td>
                    </tr>
                <?php endif ?>
            </tbody>
        </table>
    </div>
</main>

<div id="details" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <span class="close">&times;</span>
            <h2 class="title">Modal Header</h2>
        </div>
        <div class="modal-body">
        </div>
    </div>
</div>