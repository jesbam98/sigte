<main class="page-content">

    <div class="heading heading--spaced">
        <div class="heading-title">
            <h1>Proyecto: <?= $project->name ?></h1>
        </div>
    </div>

    <h2>Actividades asignadas</h2>

    <form class="assignedActivityForm" action="<?= url('assignedactivity/create') ?>" method="post">
        <input type="hidden" name="projectId" value="<?= $project->id ?>">
        <select required name="assignedActivity[activityId]" id="activities" class="assignedActivityForm-activities">
            <?php foreach ($project->getActivities() as $activity) : ?>
                <option value="<?= $activity->id ?>"><?= $activity->name ?></option>
            <?php endforeach ?>
        </select>
        <select required name="assignedActivity[memberId]" id="members" class="assignedActivityForm-members">
            <?php foreach ($team->getMembers() as $member) : ?>
                <option value="<?= $member->id ?>"><?= $member->getFullName() ?></option>
            <?php endforeach ?>
        </select>
        <button class="button button--primary">Agregar</button>
    </form>

    <div class="tableResponsive">
        <table class="table">
            <thead class="table-thead">
                <tr>
                    <th>Nombre</th>
                    <th>Encargado</th>
                    <th>Aprobada</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($assignedActivities as $assignedAct) : ?>
                    <tr>
                        <td><a href="<?= url('activity/details?id=' . $assignedAct->activityId) ?>" class="table-link"><?= $assignedAct->getActivity()->name ?></a></td>
                        <td><?= $assignedAct->getMember()->getFullName() ?></td>
                        <td>
                            <label class="switch">
                                <input class="changeStatus" type="checkbox" data-id="<?= $assignedAct->id ?>" <?= $assignedAct->getStatus() ? 'checked' : '' ?>>
                                <span class="slider"></span>
                            </label>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>

    <h3>Archivo Final</h3>
    <div class="fileForm">
        <input class="fileForm-file" type="file" name="file" required>
        <button type="submit" class="button button--secondary">Subir</button>
    </div>

</main>