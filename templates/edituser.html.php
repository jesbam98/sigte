<main class="page-content">
    <h1><?= $title ?></h1>

    <?php if (!empty($errors)) : ?>
        <div class="errorList">
            <ul>
                <?php foreach ($errors as $error) : ?>
                    <li><?= $error ?></li>
                <?php endforeach ?>
            </ul>
        </div>
    <?php endif ?>

    <form class="form" action="" method="post">
        <input type="hidden" name="user[id]" value="<?= $user->id ?? '' ?>">

        <?php if (empty($user)): ?>
        <label for="permissions" class="form-label">Tipo:</label>
        <select name="user[permissions]" id="permissions" class="form-select">
            <option value="<?= \App\Entity\User::MEMBER ?>">Alumno</option>
            <option value="<?= \App\Entity\User::SUPERVISOR ?>">Profesor</option>
        </select>
        <?php endif ?>

        <label class="form-label" for="firstName">Nombre:</label>
        <input class="form-input" type="text" name="user[firstName]" id="firstName" maxlength="100" required value="<?= $user->firstName ?? '' ?>">

        <label class="form-label" for="lastName">Apellidos:</label>
        <input class="form-input" type="text" name="user[lastName]" id="lastName" maxlength="100" required value="<?= $user->lastName ?? '' ?>">

        <label class="form-label" for="email">Correo:</label>
        <input class="form-input" type="email" name="user[email]" id="email" maxlength="100" required value="<?= $user->email ?? '' ?>">

        <label class="form-label" for="username">Usuario:</label>
        <input class="form-input" type="text" name="user[username]" id="username" minlength="6" maxlength="60" required value="<?= $user->username ?? '' ?>">

        <label class="form-label" for="password">Contraseña:</label>
        <input class="form-input" type="password" name="user[password]" id="password" minlength="6" maxlength="100" <?= $user ? '' : 'required' ?>>

        <button type="submit" class="button button--success">Guardar</button>  
              
    </form>
</main>