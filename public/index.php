<?php

/* -------------------------------------------------------------------------- */
/*                                    SIGTE                                   */
/* -------------------------------------------------------------------------- */

/**
 * Inicio de la aplicación
 * @author Jesus Bautista <jesbam98@gmail.com>
 * 
 * Todas las peticiones son redirigidas a este script
 * 
 */
try {
	include __DIR__ . '/../includes/Config.php';
	include __DIR__ . '/../includes/Helpers.php';
	include __DIR__ . '/../includes/autoload.php';

	// Obtener la ruta de la aplicación

	// Entra: /users?id=1
	// Sale: users
	$route = ltrim(strtok($_SERVER['REQUEST_URI'], '?'), '/');

	// En caso de que la ruta contenga un prefijo, este se removerá
	if (defined('URL_PREFIX') && URL_PREFIX != "" && URL_PREFIX != "/") {
		$route = str_replace_once(URL_PREFIX, '', $route);
		// Entra: sigte/users		
		// Sale: users
	}
	
	//Crear una nueva instancia de la aplicación
	$entryPoint = new \Core\EntryPoint($route, $_SERVER['REQUEST_METHOD'], new \App\AppRoutes());
	$entryPoint->run();

} catch (PDOException $e) {
	// Capturar errores de PDO
	$title = 'Ha ocurrido un error';
	
	if (DEBUG) {
		$output = 'Error de Base de Datos: ' . $e->getMessage() . ' in ' .
			$e->getFile() . ':' . $e->getLine();
	} else {
		$output = '¡Vaya! Algo salió mal. Intenta volver más tarde o contacta al administrador del sistema.';
	}

	include  __DIR__ . '/../templates/layout.html.php';
} catch (Exception $e) {
	// Capturar errores generales
	$title = 'Ha ocurrido un error';

	if (DEBUG) {
		$output = 'Error: ' . $e->getMessage() . ' in ' .
			$e->getFile() . ':' . $e->getLine();
	} else {
		$output = '¡Vaya! Algo salió mal. Intenta volver más tarde o contacta al administrador del sistema.';
	}

	include  __DIR__ . '/../templates/layout.html.php';
}
