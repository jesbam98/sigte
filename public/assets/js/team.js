function deleteMember(id, team) {
    var confirmed = confirm('¿Desea elminar este miembro?');

    if (confirmed == true) {
        $.post(BASE_URL + "team/deletemember?team=" + team, { id: id },
            function (data, textStatus, jqXHR) {
                if (data.success == true) {
                    $("#member-" + id).remove();
                } else {
                    alert('Ha ocurrido un error inesperado');
                }
            },
            "json"
        );
    }
}

function setAsLeader(id, team) {
    $.post(BASE_URL + "team/setleader?team=" + team, { id: id },
        function (data, textStatus, jqXHR) {
            if (data.success == true) {
                window.location.reload();
            } else {
                alert('Ha ocurrido un error inesperado');
            }
        },
        "json"
    );
}

$(function () {
    $('#dueDate').flatpickr({
        "locale": "es"
    });

    $('#members').on('click', '.memberList-delete', function () {
        let button = $(this);
        let id = button.data('id');
        let teamId = button.data('team');

        if (id && teamId) {
            deleteMember(id, teamId);
        }
    });

    $('#members').on('click', '.setAsLeader', function () {
        let button = $(this);
        let id = button.data('id');
        let teamId = button.data('team');

        if (id && teamId) {
            setAsLeader(id, teamId);
        }
    });
});