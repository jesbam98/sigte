$(function () {
    $('.changeStatus').on('change', function () {
        let input = this;
        let status = input.checked ? 1 : 0;

        if (input.dataset.id) {
            let id = input.dataset.id;

            $.post(BASE_URL + "activity/assigned/status", {
                id: id,
                status: status
            },
                function (data, textStatus, jqXHR) {
                    if (!data.success) {
                        alert('Ha ocurrido un error');
                    }
                },
                "json"
            );

        }

    });
});